/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrabble;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Romain
 */
public class Cases {
    static Map<String,List<String>> m ;
    static List<String> L1; 
    static List<String> L2;
    static List<String> L3;
    static List<String> L4;
    static List<String> L5;
    static List<String> L6;
    static List<String> L7;
    static List<String> L8;
    static List<String> L9; 
    static List<String> L10;
    static List<String> L11;
    static List<String> L12;
    static List<String> L13;
    static List<String> L14;
    static List<String> L15;
    
    static String[] A = {"MT","SI","SI","LD","SI","SI","SI","MT","SI","SI","SI","LD","SI","SI","MT"};
    static String[] B = {"SI","MD","SI","SI","SI","LT","SI","SI","SI","LT","SI","SI","SI","MD","SI"};
    static String[] C = {"SI","SI","MD","SI","SI","SI","LD","SI","LD","SI","SI","SI","MD","SI","SI"};
    static String[] D = {"LD","SI","SI","MD","SI","SI","SI","LD","SI","SI","SI","MD","SI","SI","LD"};
    static String[] E = {"SI","SI","SI","SI","MD","SI","SI","SI","SI","SI","MD","SI","SI","SI","SI"};
    static String[] F = {"SI","LT","SI","SI","SI","LT","SI","SI","SI","LT","SI","SI","SI","LT","SI"};
    static String[] G = {"SI","SI","LD","SI","SI","SI","LD","SI","LD","SI","SI","SI","LD","SI","SI"};
    static String[] H = {"MT","SI","SI","LD","SI","SI","SI","CE","SI","SI","SI","LD","SI","SI","MT"};
    static String[] I = {"SI","SI","LD","SI","SI","SI","LD","SI","LD","SI","SI","SI","LD","SI","SI"};
    static String[] J = {"SI","LT","SI","SI","SI","LT","SI","SI","SI","LT","SI","SI","SI","LT","SI"};
    static String[] K = {"SI","SI","SI","SI","MD","SI","SI","SI","SI","SI","MD","SI","SI","SI","SI"};
    static String[] L = {"LD","SI","SI","MD","SI","SI","SI","LD","SI","SI","SI","MD","SI","SI","LD"};
    static String[] M = {"SI","SI","MD","SI","SI","SI","LD","SI","LD","SI","SI","SI","MD","SI","SI"};
    static String[] N = {"SI","MD","SI","SI","SI","LT","SI","SI","SI","LT","SI","SI","SI","MD","SI"};
    static String[] O = {"MT","SI","SI","LD","SI","SI","SI","MT","SI","SI","SI","LD","SI","SI","MT"};
    
    public static void createTab(){
        m = new TreeMap<>();
        L1 = new ArrayList<>();
        for(String s : A){
            L1.add(s);
        }
        L2 = new ArrayList<>();
        for(String s : B){
            L2.add(s);
        }
        L3 = new ArrayList<>();
        for(String s : C){
            L3.add(s);
        }
        L4 = new ArrayList<>();
        for(String s : D){
            L4.add(s);
        }
        L5 = new ArrayList<>();
        for(String s : E){
            L5.add(s);
        }
        L6 = new ArrayList<>();
        for(String s : F){
            L6.add(s);
        }
        L7 = new ArrayList<>();
        for(String s : G){
            L7.add(s);
        }
        L8 = new ArrayList<>();
        for(String s : H){
            L8.add(s);
        }
        L9 = new ArrayList<>();
        for(String s : I){
            L9.add(s);
        }
        L10 = new ArrayList<>();
        for(String s : J){
            L10.add(s);
        }
        L11 = new ArrayList<>();
        for(String s : K){
            L11.add(s);
        }
        L12 = new ArrayList<>();
        for(String s : L){
            L12.add(s);
        }
        L13 = new ArrayList<>();
        for(String s : M){
            L13.add(s);
        }
        L14 = new ArrayList<>();
        for(String s : N){
            L14.add(s);
        }
        L15 = new ArrayList<>();
        for(String s : O){
            L15.add(s);
        }
        
        
        m.put("A",L1);
        m.put("B",L2);
        m.put("C",L3);
        m.put("D",L4);
        m.put("E",L5);
        m.put("F",L6);
        m.put("G",L7);
        m.put("H",L8);
        m.put("I",L9);
        m.put("J",L10);
        m.put("K",L11);
        m.put("L",L12);
        m.put("M",L13);
        m.put("N",L14);
        m.put("O",L15);
        
    
    }
    
}
