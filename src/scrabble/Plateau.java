/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrabble;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static scrabble.Cases.m;

/**
 *
 * @author Romain
 */
public class Plateau {

    public static void dessiner() {
        Cases.createTab();
        System.out.print("  |");
        for (int i = 1; i <= 15; ++i) {
            if(i<10)
            System.out.print("  " + i + "|");
            else
                System.out.print(" "+ i + "|");
        }
        System.out.println("");
        System.out.println("----------------------------------------------------------------");

        for (String S : Cases.m.keySet()) {
            System.out.print(S+" |");
            for (String t : Cases.m.get(S)) {
                
                
                if (t.equals("MT")) {
                    
                    System.out.print(Multiplicateur.MT);
                }
                else if(t.equals("SI")){
                    
                    System.out.print(Multiplicateur.SI);
                }
                else if(t.equals("MD")){
                    
                    System.out.print(Multiplicateur.MD);
                }
                else if(t.equals("LT")){
                    
                    System.out.print(Multiplicateur.LT);
                }
                else if(t.equals("LD")){
                    
                    System.out.print(Multiplicateur.LD);
                }
                else if(t.equals("CE")){
                    
                    System.out.print(Multiplicateur.CE);
                }
                else
                    System.out.print(" " + t + " |");
                

            }
            System.out.println("");
            System.out.println("----------------------------------------------------------------");
        }

    }
    
    
    public static void ajouterLettre(String s, Integer i, String l){
        if(s.equals("A")){
            Cases.A[i-1] = l;
        }
        else if(s.equals("B")){
            Cases.B[i-1] = l;
        }
        else if(s.equals("C")){
            Cases.C[i-1] = l;
        }
        else if(s.equals("D")){
            Cases.D[i-1] = l;
        }
        else if(s.equals("E")){
            Cases.E[i-1] = l;
        }
        else if(s.equals("F")){
            Cases.F[i-1] = l;
        }
        else if(s.equals("G")){
            Cases.G[i-1] = l;
        }
        else if(s.equals("H")){
            Cases.H[i-1] = l;
        }
        else if(s.equals("I")){
            Cases.I[i-1] = l;
        }
        else if(s.equals("J")){
            Cases.J[i-1] = l;
        }
        else if(s.equals("K")){
            Cases.K[i-1] = l;
        }
        else if(s.equals("L")){
            Cases.L[i-1] = l;
        }
        else if(s.equals("M")){
            Cases.M[i-1] = l;
        }
        else if(s.equals("N")){
            Cases.N[i-1] = l;
        }
        else if(s.equals("O")){
            Cases.O[i-1] = l;
        }
        Plateau.dessiner();
        
        
}}
